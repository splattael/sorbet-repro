# Reproduction for https://github.com/sorbet/sorbet/issues/7636

```shell
git clone https://gitlab.com/splattael/sorbet-repro
cd sorbet-repro
ruby -v
# ruby 3.0.5p211 (2022-11-24 revision ba5cf0f7c5) [x86_64-linux]
gem install bundler
bundle
srb --version
# Sorbet typechecker 0.5.11218 git a4457bfc25b26f8953eef8e529ec5a7488f2e1ac debug_symbols=true clean=0
srb tc lib/
./lib/foo.rb:4: Duplicate type member Elem https://srb.help/4011
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^
    lib/foo.rb:4: Also defined here
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^

./lib/foo.rb:4: Duplicate type member Elem https://srb.help/4011
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^
    lib/foo.rb:4: Also defined here
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^

lib/foo.rb:4: Duplicate type member Elem https://srb.help/4011
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^
    ./lib/foo.rb:4: Also defined here
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^

lib/foo.rb:4: Duplicate type member Elem https://srb.help/4011
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^
    ./lib/foo.rb:4: Also defined here
     4 |  Bar = Struct.new(:x)
          ^^^^^^^^^^^^^^^^^^^^
Errors: 4
```
